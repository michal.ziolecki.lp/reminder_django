from uuid import uuid4
from django.db import models
from django.urls import reverse
from django.utils import timezone

from user.models import User


class Event(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid4,
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=False, null=False)
    event_date = models.DateField(unique=False, null=False, default=timezone.now)
    event_time = models.TimeField(unique=False, null=False, default=timezone.now)
    annually = models.BooleanField(default=True, null=False)
    place = models.CharField(max_length=100, unique=False, null=True)
    description = models.CharField(max_length=250, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('event-details', kwargs={
            'pk': self.pk
        })

    class Meta:
        ordering = ['event_date']
