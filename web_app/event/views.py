from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, ListView, DetailView, DeleteView, UpdateView

from event.forms import EventCreateForm
from event.models import Event
from user.models import User


class CreateEvent(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventCreateForm
    template_name = 'event/create_event.html'
    success_url = reverse_lazy('event:event-list')

    def form_valid(self, form):
        event = form.save(commit=False)
        event.user = User.objects.get(pk=self.request.user.pk)
        return super(CreateEvent, self).form_valid(form)


class ListEvent(LoginRequiredMixin, ListView):
    model = Event
    template_name = 'event/list_event.html'
    paginate_by = 5

    def get_queryset(self):
        queryset = super(ListEvent, self).get_queryset()
        return queryset.filter(user__pk=self.request.user.pk).order_by('event_date')


class EventDetails(LoginRequiredMixin, DetailView):
    model = Event
    template_name = 'event/details_event.html'

    def get_object(self, queryset=None):
        event_pk = self.kwargs.get('pk', '')
        event_obj = Event.objects.filter(pk=event_pk).get()
        if event_obj.user.pk != self.request.user.pk:
            raise PermissionDenied()
        return super().get_object(queryset)


class DeleteEvent(LoginRequiredMixin, DeleteView):
    model = Event
    success_url = reverse_lazy('event:event-list')

    def get_object(self, queryset=None):
        event_pk = self.kwargs.get('pk', '')
        event_obj = Event.objects.filter(pk=event_pk).get()
        if event_obj.user.pk != self.request.user.pk:
            raise PermissionDenied()
        return super().get_object(queryset)


class UpdateEvent(LoginRequiredMixin, UpdateView):
    model = Event
    success_url = reverse_lazy('event:event-list')
    form_class = EventCreateForm
    template_name = 'event/update_event.html'

    def form_valid(self, form):
        event = form.save(commit=False)
        event.user = User.objects.get(pk=self.request.user.pk)
        return super(UpdateEvent, self).form_valid(form)

    def get_object(self, queryset=None):
        event_pk = self.kwargs.get('pk', '')
        event_obj = Event.objects.filter(pk=event_pk).get()
        if event_obj.user.pk != self.request.user.pk:
            raise PermissionDenied()
        return super().get_object(queryset)

