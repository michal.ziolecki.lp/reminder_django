from django.forms import ModelForm, DateTimeInput
from event.models import Event


class EventCreateForm(ModelForm):
    class Meta:
        model = Event
        exclude = ['user']
        fields = [
            'name',
            'event_date',
            'event_time',
            'place',
            'description',
            'annually'
        ]
        widgets = {
            'event_date': DateTimeInput(attrs={'type': 'date'}),
            'event_time': DateTimeInput(attrs={'type': 'time'}),
        }
