from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, DeleteView, UpdateView
from django.contrib.auth.views import LoginView
from django.shortcuts import render
from .forms import UserCreateForm, UserUpdatePwdForm
from .models import User, ConfirmationToken


class RegisterUserView(CreateView):
    form_class = UserCreateForm
    success_url = reverse_lazy('home')
    template_name = 'user/user_register.html'
    success_message = 'Check you email and confirm registration'

    def form_valid(self, form):

        context = {'form': form}
        username = form.cleaned_data['username']
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        repeat_password = form.cleaned_data['repeat_password']

        if password != repeat_password:
            form.error = 'Passwords are not the same'
            return render(self.request, self.template_name, context)
        if email and User.objects.filter(email=email).exists():
            form.error = 'Email is occupied'
            return render(self.request, self.template_name, context)
        if username and User.objects.filter(username=username).exists():
            form.error = 'Username is occupied'
            return render(self.request, self.template_name, context)

        user = form.save(commit=False)
        user.set_password(password)
        user.is_active = False
        return super(RegisterUserView, self).form_valid(form)


class LoginUserView(LoginView):
    template_name = 'user/user_login.html'
    redirect_authenticated_user = True
    success_url = reverse_lazy('home')


class ActivateView(View):
    def get(self, request, token):
        try:
            token_entity = ConfirmationToken.objects.filter(token=token).select_related(
                'user').get()
            user = token_entity.user
            user.is_active = True
            User.objects.filter(pk=user.pk).update(is_active=True)
        except ConfirmationToken.DoesNotExist:
            raise Http404('Address does not exist')
        return render(request, template_name='user/sing_up_confirmed.html', context={})


class AccountView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, template_name='user/account_view.html', context={})


class DeleteAccount(LoginRequiredMixin, DeleteView):
    model = User
    template_name = 'user/user_confirm_delete.html'
    success_url = reverse_lazy('home')


class UserUpdatePassword(LoginRequiredMixin, UpdateView):
    model = User
    success_url = reverse_lazy('home')
    form_class = UserUpdatePwdForm
    template_name = 'user/user_password.html'

    def form_valid(self, form):
        # TODO checking old password and create new
        return super(UserUpdatePassword, self).form_valid(form)
