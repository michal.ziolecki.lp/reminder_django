from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.conf import settings
from django.urls import reverse
from django.utils.encoding import force_bytes
from smtplib import SMTPException
from .models import User, ConfirmationToken
from logging import Logger
import logging
import base64


def send_confirmation_email(user: User, token: ConfirmationToken) -> bool:
    logger: Logger = logging.getLogger(settings.LOGGER_NAME)
    domain = settings.WEB_DOMAIN
    link = reverse('confirm-account', kwargs={
        'token': token.token,
    })
    protocol = 'http://'
    if settings.HTTP_PROTOCOL != 'HTTP':
        protocol = 'https://'
    activate_url = f'{protocol}{domain}:{settings.PORT}{link}'

    subject = 'Confirm account email - ReminderApp'
    message = f'Hello,\n thank you for your sign up.\nConfirm your account by this link:\n{activate_url}'
    context = {
        'subject': subject,
        'message': message,
        'activate_url': activate_url
    }
    recipient = user.email
    email = EmailMultiAlternatives(subject=subject,
                                   body=message,
                                   from_email=settings.EMAIL_HOST_USER,
                                   to=[recipient])
    html_template = get_template('user/sign_up_email.html').render(context=context)
    email.attach_alternative(html_template, 'text/html')

    try:
        email.send(fail_silently=False)
    except SMTPException as e:
        logger.error(f'{__name__}: Error while sending email, info: {e.args}')
        return False
    return True
