from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.db import models
from uuid import uuid4
from django.db.models.signals import post_save
from django.conf import settings


class User(AbstractUser):

    def get_absolute_url(self):
        return reverse('account-details', kwargs={'pk': self.id})

    def __str__(self):
        return self.username


class ConfirmationToken(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )
    token = models.UUIDField(
        primary_key=True,
        default=uuid4,
        editable=False)


def create_token_and_send_confirm_email_post_save(sender, instance, **kwargs):
    from .tasks import task_send_confirmation_email
    if not instance.is_active:
        if ConfirmationToken.objects.filter(user=instance).exists():
            ConfirmationToken.objects.get(user=instance).delete()
        token = ConfirmationToken(user=instance)
        token.save()
        task_send_confirmation_email.delay(instance.pk, token.token)


post_save.connect(create_token_and_send_confirm_email_post_save, sender=User)
