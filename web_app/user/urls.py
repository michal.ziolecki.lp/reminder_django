from django.urls import path, include
from .views import RegisterUserView, LoginUserView, ActivateView, AccountView, DeleteAccount, UserUpdatePassword
from django.contrib.auth.views import LogoutView

app_name = 'user'

urlpatterns = [
    path('register/', RegisterUserView.as_view(), name='register'),
    path('login/', LoginUserView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('account/', AccountView.as_view(), name='account'),
    path('account/password/<pk>', UserUpdatePassword.as_view(), name='account-password'),
    path('account/delete/<pk>', DeleteAccount.as_view(), name='account-delete'),
    path('confirm-account/<token>/', ActivateView.as_view(), name='confirm-account'),
]
