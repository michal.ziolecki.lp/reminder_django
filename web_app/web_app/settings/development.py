from .base import *

INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

INTERNAL_IPS = ('127.0.0.1',)

# sending e-mails - locally debuging method
# command for run simulator: python -m smtpd -n -c DebuggingServer localhost:2525
EMAIL_USE_SSL = False
EMAIL_USE_TLS = False
EMAIL_HOST = '127.0.0.1'
EMAIL_PORT = 2525
EMAIL_HOST_USER = config('EMAIL_ACCOUNT')

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# logging
LOGGER_NAME = 'ReminderLogger'
U_LOGFILE_SIZE = 1 * 1024 * 1024
U_LOGFILE_COUNT = 2
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{name} - {asctime} - {module} - {levelname} {message}',
            'style': '{',
        },
        'basic': {
            'format': '{name} - {levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file': {
            'level': config('LOG_LEVEL', cast=str),
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs', 'develop_logs.log'),
            'maxBytes': U_LOGFILE_SIZE,
            'backupCount': U_LOGFILE_COUNT,
            'formatter': 'verbose'
        },
        'console': {
            'level': config('LOG_LEVEL', cast=str),
            'class': 'logging.StreamHandler',
            'formatter': 'basic'
        },
    },
    'loggers': {
        'ReminderLogger': {
            'handlers': ['file', 'console'],
            'level': config('LOG_LEVEL', cast=str),
            'propagate': True,
        },
    },
}
